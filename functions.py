from collections import Counter
from itertools import permutations
from sklearn.preprocessing import MinMaxScaler
from sklearn import preprocessing
from sklearn import cluster,metrics
from keras.layers import *
from keras.models import Model, Sequential
from keras.regularizers import l2
from keras import backend as K
from tensorflow.keras.optimizers import SGD, Adam, Adadelta
from keras.losses import binary_crossentropy
from tensorflow.keras.utils import plot_model
from sklearn.mixture import GaussianMixture
from scipy.io import wavfile
from scipy import spatial
from tensorflow.keras.applications.resnet50 import ResNet50, preprocess_input
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping
from transformers import Wav2Vec2ForPreTraining, Wav2Vec2FeatureExtractor, Wav2Vec2Tokenizer, Wav2Vec2ForCTC
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import f1_score
from python_speech_features import mfcc, logfbank, fbank, sigproc
from ipywidgets import interact, interactive, fixed, interact_manual
import ipywidgets as widgets


import matplotlib.pyplot as plt
import keras as k
import os
import tensorflow as tf
import keras
import soundfile as sf
import random
import itertools
import numpy as np
import pandas as pd
import pickle
import scipy.io.wavfile as wav
import librosa
import scipy
import math
import sklearn
import torch
import torchaudio
import openl3
import IPython

def resample_file(file):
    data,sr = librosa.load(file,sr=16000)
    sf.write(file,data,sr)

def create_spectrogram_features(filepath):
    sig, sr = sf.read(filepath)
    spec = librosa.feature.melspectrogram(y=sig, sr=sr, hop_length=320, win_length=400)
    spec_db = librosa.amplitude_to_db(spec, ref=np.min)
    spec_db = pd.DataFrame(spec_db)
    spec_db_filename =  filepath.split('.')[0]+'_SpecDB'
    spec_db.to_pickle(spec_db_filename)
    return(spec_db)

def create_openl3_features(filepath):
    audio, sr  = sf.read(filepath)
    emb, ts = openl3.get_audio_embedding(audio, sr, content_type = 'env', embedding_size=512, verbose=False, frontend='kapre')
    emb = list(emb)
    first_ele = emb.pop(0)
    emb = pd.DataFrame(emb)
    openl3_filename =  filepath.split('.')[0]+'_OpenL3'
    emb.to_pickle(openl3_filename)
    return(emb)

def wav_collate_fn(file, w2v2tokenizer):
    return w2v2tokenizer(file, sampling_rate = 16000, return_tensors="pt").input_values

def get_w2v2_vector(audio, device, w2v2_model, w2v2tokenizer, feature_type = 'last'):
    with torch.no_grad():
        to_feed = wav_collate_fn(audio, w2v2tokenizer).to(device).to(torch.float)
        #either take output, penultimate output or average
        cur_out = w2v2_model(to_feed, output_hidden_states=True).hidden_states
        if feature_type == 'last':
            cur_out = cur_out[0]
        elif feature_type == 'penultimate':
            cur_out = cur_out[-1]
        elif feature_type == 'mean':
            cur_out = torch.mean(torch.stack(cur_out), dim=0)
    return(cur_out)

def get_pooled_vector(w2v2_vector,pooling):
    if pooling == 'first':
        w2v2_vector = w2v2_vector[0]
    elif pooling == 'last':
        w2v2_vector = w2v2_vector[-1]
    elif pooling == 'middle':
        w2v2_vector =  w2v2_vector[len(w2v2_vector)//2]
    elif pooling == 'mean':
        w2v2_vector = w2v2_vector.mean(axis=0)
    return(w2v2_vector)

def create_w2v2_features(filepath, device, w2v2_model, w2v2tokenizer, feature_type = 'last'):    
    sig, sr = sf.read(filepath)
    w2v2_features = pd.DataFrame()
    i = 0 
    while i<(len(sig)-16000):
        audio = sig[i:i+16080]        
        w2v2_vector = get_w2v2_vector(audio, device = device, w2v2_model = w2v2_model, w2v2tokenizer = w2v2tokenizer, feature_type='last').cpu()
        w2v2_vector = w2v2_vector.numpy()[0] 
        w2v2_vector = pd.DataFrame(w2v2_vector)
        w2v2_features = w2v2_features.append(w2v2_vector)
        i+=16000

    audio = list(sig[i:len(sig)])
    pad = [0]*(400-(len(sig)-(i+320*((len(sig)-i)//320))))
    audio = np.asarray(audio+pad)
    w2v2_vector = get_w2v2_vector(audio, device = device, w2v2_model = w2v2_model, w2v2tokenizer = w2v2tokenizer, feature_type='last').cpu()  
    w2v2_vector = w2v2_vector.numpy()[0]
    w2v2_vector = pd.DataFrame(w2v2_vector)
    w2v2_features = w2v2_features.append(w2v2_vector)    

    w2v2_filename =  filepath.split('.')[0]+'_W2V2'
    w2v2_features.to_pickle(w2v2_filename)

    return(w2v2_features)

def create_pooled_features(filepath, pooling='mean'):
    logspec_dataset = []
    w2v2_dataset = pd.DataFrame()    
    
    name = filepath.split('.')[0]
    logspec_file = name + '_SpecDB'
    w2v2_file = name + '_W2V2'

    logspec = pd.read_pickle(logspec_file)
    logspec = logspec.T
    w2v2 = pd.read_pickle(w2v2_file)

    i = 0
    while i< len(w2v2)-25:

        logspec_vector = logspec[i:i+25]
        w2v2_vector = w2v2[i:i+25]

        w2v2_vector = get_pooled_vector(w2v2_vector,pooling)
        w2v2_dataset = w2v2_dataset.append(w2v2_vector,ignore_index=True)
        logspec_dataset.append(logspec_vector.values.tolist())
        i+=5

    logspec_filename = name + '_SpecDB_Final'
    w2v2_filename = name + '_W2V2_Final'

    with open(logspec_filename, 'wb') as fp: pickle.dump(logspec_dataset, fp)
    w2v2_dataset.to_pickle(w2v2_filename)

    return(logspec_dataset,w2v2_dataset)

def compute_bic(X):
    gm = GaussianMixture(n_components=2, random_state=1, covariance_type ='diag').fit(X)
    BIC = gm.bic(X)
    return(BIC)

def gen_mfcc(file, numcep = 13):
    (rate,sig) = wav.read(file)
    data = mfcc(sig,rate,numcep=numcep)
    return(data)

def gen_melfb(file, nfilt = 26):
    (rate,sig) = wav.read(file)
    data,energy = fbank(sig,rate,nfilt=nfilt)
    return(data)

def gen_openl3(file):
    (rate,sig) = wav.read(file)
    data, ts = openl3.get_audio_embedding(sig,rate,embedding_size=512)
    return(data)

def bic_sd_boundaries(file):        
        #BIC Boundaries
        threshold = [300]
        files = [file]

        time_change = bic_bounds(threshold,files)        

        #Silence Likelihood Boundaries
        x,sr,rms_db,times = find_rms(file)
        gt,lt = find_thresholds(x,sr)
        silence = silence_likelihood(rms_db,times,gt,lt)
        silence_boundaries,break_boundaries = silence_boundary_detection(silence,times)

        #Combination
        combined_boundaries = combine_boundaries(silence_boundaries,break_boundaries,time_change)
        return(combined_boundaries)
    
def bic_bounds(threshold,files,a_hop=50,b_hop=100,t_hop=10,frame_duration=0.01,feature='mfcc'):
    labels = ['TT','MS','GR','CT','S','N','SN']

    for p in range(len(threshold)):
        for k in range(len(files)):
            #Extracting Data from files
            if(feature == 'mfcc'):
                data = gen_mfcc(files[k])
            elif(feature == 'melfb'):
                data = gen_melfb(files[k])
            elif(feature == 'openl3'):
                data = gen_openl3(files[k])
                a_hop = 5
                b_hop = 10
                t_hop = 1
                frame_duration = 0.1
                
            else:
                print("Feature not defined")
                
            scaler = MinMaxScaler()
            scaler.fit(data)
            data = scaler.transform(data)

            a=0
            t=a+a_hop
            b=a+b_hop

            count=0    
            time_change = []
            difference = []
            times = []             
            count_true = 0            
            
            while (b<len(data)):                 
                
                X1= data[a:t]
                X2= data[t:b]
                X=  data[a:b]

                if(X2.shape[0]<a_hop):
                    break        

                bic1 = compute_bic(X1)
                bic2 = compute_bic(X2) 
                bic = compute_bic(X)
                
                diff = abs((bic1+bic2)-bic)
                difference.append(diff)
                times.append(round((t*frame_duration),2))

                a+=5
                t=a+a_hop
                b=a+b_hop
            
            i = 0            
            time_change = []
            while i<(len(times)-10):
                window = times[i:i+10]
                diff = difference[i:i+10]

                change_diff = max(diff)
                change_ind = diff.index(change_diff)

                if(change_diff>=threshold[p]):
                    time_change.append(window[change_ind])
                    i = i+change_ind+10 

                else:
                    i+=1        
       
    return(time_change)

def find_rms(filename):    
    x, sr = librosa.load(filename,sr=None)
    rms = librosa.feature.rms(x, frame_length=400, hop_length=160, center=True)
    rms = rms[0]
    rms_db = 10*np.log(rms)
    times = librosa.times_like(rms,sr=sr, hop_length=160)
    return(x,sr,rms_db,times)
    
def find_thresholds(x,sr):
    rms = librosa.feature.rms(x, frame_length=400, hop_length=160, center=True)
    rms = rms[0]
    rms_db = 10*np.log(rms)
    times = librosa.times_like(rms,sr=sr, hop_length=160)    
    
    global_threshold = -50
    global_thr = []
    local_thr = []
    
    for i in range(len(times)):        
        if(i<=100):
            global_list = rms_db[0:i+500]
            local_list = rms_db[0:i+100]
            
        elif(i>100 and i<=500):
            global_list = rms_db[0:i+500]
            local_list = rms_db[i-100:i+100]
            
        elif(len(times)-i-1<=500 and len(times)-i-1>100):
            global_list = rms_db[i-500:len(times)-1]
            local_list = rms_db[i-100:i+100]
            
        elif(len(times)-i-1<=100):
            global_list = rms_db[i-500:len(times)-1]
            local_list = rms_db[i-100:len(times)-1]
            
        else:
            global_list = rms_db[i-500:i+500]
            local_list = rms_db[i-100:i+100]
            
        if(np.percentile(global_list,2)+10<global_threshold):
            global_thr.append(global_threshold)
        else:
            global_thr.append(np.percentile(global_list,2)+10)        
        
        local_thr.append(np.percentile(global_list,50)-10)
            
    return(global_thr,local_thr) 
    
def silence_likelihood(rms_db,times,global_thr,local_thr):
    silence = np.zeros(len(rms_db))
    for i in range(len(times)):
        if (rms_db[i]<global_thr[i] or rms_db[i]<local_thr[i]):
            silence[i] = 1
    return(silence)
    
def silence_boundary_detection(silence,times):
    time_silence = [x for x, y in zip(times, silence) if y]
    ts = np.round(time_silence,1)
    i = 0
    j = 0 
    silence_boundaries = []
    break_boundaries = []
    while i<(len(ts)-2):
        if(np.round(ts[i+1]-ts[i],1)<=0.1):        
            j=i
            while j<(len(ts)-2):            
                if(np.round(ts[j+1]-ts[j],1)>0.1):                
                    break
                else:
                    j+=1
            if(time_silence[j]-time_silence[i]>0.2):
                silence_boundaries.append(time_silence[i])
                silence_boundaries.append(time_silence[j])
            elif(time_silence[j]-time_silence[i]<=0.2 and time_silence[j]-time_silence[i]>=0.04):
                break_boundaries.append(round(((time_silence[j]+time_silence[i])/2),1))            
            i = j
        else:
            i+=1
    
    return(silence_boundaries,break_boundaries)
    
def combine_boundaries(silence_boundaries,break_boundaries,time_change):
    combined_boundaries = np.round(silence_boundaries + break_boundaries + time_change,1)
    combined_boundaries.sort()
    combined_boundaries = combined_boundaries.tolist()
    i = 0
    while i<(len(combined_boundaries)-1):        
        if(np.round(combined_boundaries[i+1]-combined_boundaries[i],1)<0.5):
            combined_boundaries[i] = np.round((combined_boundaries[i]+combined_boundaries[i+1])/2,1)
            combined_boundaries.pop(i+1)
        else:
            i+=1       
    return(combined_boundaries)    

def create_label_track(boundaries,filename):
    label_track = pd.DataFrame()
    for i in range(len(boundaries)-1):
        label_list = []
        label_list.append(boundaries[i])
        label_list.append(boundaries[i+1])
        label_list.append(' ')
        label_track = label_track.append([label_list])     
    np.savetxt(filename, label_track.values,fmt="%s", delimiter='\t')

def frame_to_time(frame,hop_size=0.1):
  return frame*hop_size

def generate_framewise_lt_predictions(model, filepath, classes):
    speech_classes = ['CT','GR','MS','TT']
    noise_classes = ['S','STR']
    threshold = [0.45, 0.55, 0.6, 0.05, 0.45, 0.1, 0.3, 0.7]
    
    name = filepath.split('.')[0]
    logspec_file = name + '_SpecDB_Final'
    w2v2_file = name + '_W2V2_Final'
    openl3_file = name + '_OpenL3' 

    with open(logspec_file, "rb") as fp: logspec = pickle.load(fp)  
    w2v2 = pd.read_pickle(w2v2_file)
    openl3 = pd.read_pickle(openl3_file)

    X = np.array([np.array(x).T.reshape( (128, 25, 1) ) for x in logspec])
    del logspec
    X = tf.convert_to_tensor(X)
    w2v2 = tf.convert_to_tensor(w2v2)
    openl3 = tf.convert_to_tensor(openl3)

    predict = model.predict(x=[X,w2v2,openl3])
    prediction = np.zeros(predict.shape)

    for i in range(len(prediction)):
        
        contour = np.max(predict[i])
        speech_probs = [predict[i][0],predict[i][1],predict[i][2],predict[i][7]] 
        noise_probs = [predict[i][4],predict[i][6]] 
        speech_highest = sorted(range(len(speech_probs)), key=lambda i: speech_probs[i], reverse=True)[:2]
        noise_highest = np.argmax(noise_probs)
        speech_classes_present = [speech_classes[i] for i in speech_highest]
        noise_classes_present = noise_classes[noise_highest]
        classes_present = speech_classes_present.copy()
        classes_present.append(noise_classes_present)
        classes_index = [classes.index(i) for i in classes_present]
        
        for j in classes_index:
            if predict[i][j] > (contour*threshold[j]):     
                prediction[i][j] = 1
    
        if predict[i][4]>0.7 and prediction[i][4] == 1:
            prediction[i] = np.array([0,0,0,0,1,0,0,0])


    prediction_name = name + '_LT_Prediction'
    with open(prediction_name, 'wb') as fp: pickle.dump(prediction, fp)
    return(prediction)

def generate_framewise_predictions(model, filepath, classes, threshold):
    speech_classes = ['CT','GR','MS','TT']
    noise_classes = ['S','STR']
    threshold = [0.2, 0.35, 0.3, 0.05, 0.2, 0.05, 0.2, 0.35]
    
    name = filepath.split('.')[0]
    logspec_file = name + '_SpecDB_Final'
    w2v2_file = name + '_W2V2_Final'
    openl3_file = name + '_OpenL3' 

    with open(logspec_file, "rb") as fp: logspec = pickle.load(fp)  
    w2v2 = pd.read_pickle(w2v2_file)
    openl3 = pd.read_pickle(openl3_file)

    X = np.array([np.array(x).T.reshape( (128, 25, 1) ) for x in logspec])
    del logspec
    X = tf.convert_to_tensor(X)
    w2v2 = tf.convert_to_tensor(w2v2)
    openl3 = tf.convert_to_tensor(openl3)

    predict = model.predict(x=[X,w2v2,openl3])
    prediction = np.zeros(predict.shape)

    for i in range(len(prediction)):    
         for j in range(len(classes)):
            if predict[i][j] > (threshold[j]):     
                prediction[i][j] = 1
    
    prediction_name = name + '_Prediction'
    with open(prediction_name, 'wb') as fp: pickle.dump(prediction, fp)
    return(prediction)

def time_to_frame(time):
    return(time*10)

def most_common(lst):
    return max(set(lst), key=lst.count)

def create_labels(filepath, annotation_file, sr=16000, hop=320):
    sig, sr = sf.read(filepath)
    boundaries = pd.read_csv(annotation_file, delimiter='\t', header=None)
    boundaries[0] = (boundaries[0].round(decimals = 2)*sr).astype(int)
    boundaries[1] = (boundaries[1].round(decimals = 2)*sr).astype(int)

    labels_true = []
    classes = ['CT','GR','MS','N','S','SN','STR','TT']

    for i in range(0,len(sig),hop):
        labels_frame = [0]*len(classes)
        labels_true.append(labels_frame)

    labels_true = pd.DataFrame(labels_true)

    for j in range(len(boundaries)):    
        start = boundaries[0][j]
        end = boundaries[1][j]
        label = "".join(str(boundaries[2][j]).split())

        if label in classes:                
            ind = classes.index(label)    
            start_sample = start//hop
            end_sample = end//hop       

            if end_sample>=len(labels_true):
                labels_true.loc[range(start_sample,len(labels_true)),[ind]] = 1

            else:
                labels_true.loc[range(start_sample,end_sample+1),[ind]] = 1 

        else:
            continue
    return(labels_true)

def create_framewise_labels(filepath, annotation_file, sr=16000, hop=320):
    labels_dataset = pd.DataFrame()
    labels = create_labels(filepath, annotation_file, sr=16000, hop=320)
    i = 0

    while i< len(labels)-25:
        labels_vector = labels[i:i+25]
        labels_temp = np.array([0]*8)

        for j in range(len(labels_vector)-1):
            labels_current = np.array(labels_vector.iloc[j].tolist())
            labels_temp = labels_temp | labels_current

        labels_dataset = labels_dataset.append(pd.Series(labels_temp),ignore_index=True)
        i+=5

    labels_dataset = labels_dataset.astype(int)
    return(labels_dataset)

def generate_label_track(filepath):
    name = filepath.split('.')[0]

    prediction_filename = name + '_LT_Prediction'
    init_boundaries_filename = name + '_Init.txt'
    postproc_preds_filename = name + '_Label_Track.txt'

    prediction = pd.DataFrame(pd.read_pickle(prediction_filename))
    init_boundaries = pd.read_csv(init_boundaries_filename, delimiter='\t', header=None)

    classes = ['CT','GR','MS','N','S','SN','STR','TT']
    speech_classes = ['CT','GR','MS','TT']
    noise_classes = ['N','S','SN']     
    classes_present = []
    speech_present = []
    noise_present = []

    postproc_preds = pd.DataFrame()

    for i in range(len(init_boundaries)):

        segment = init_boundaries.loc[i,init_boundaries.columns]
        onset = np.clip(time_to_frame(segment[0]),a_min=0,a_max=len(prediction))
        offset = np.clip(time_to_frame(segment[1]),a_min=0,a_max=len(prediction))

        preds = prediction.loc[onset:offset-1,prediction.columns]
        preds = list(preds.to_numpy())
        preds = [tuple(line) for line in preds]        

        if len(preds)>0:                         
            pred_max = np.array(most_common(preds))
            indices = np.where(pred_max == 1)[0]
            classes_present = [classes[i] for i in indices]
            speech_present = list(set.intersection(set(classes_present), set(speech_classes)))
            noise_present = list(set.intersection(set(classes_present), set(noise_classes)))

            for class_present in classes_present:                
                segment_preds = pd.DataFrame({0:[segment[0]],1:[segment[1]],2:[class_present]})
                postproc_preds = postproc_preds.append(segment_preds)

    postproc_preds.to_csv(postproc_preds_filename,sep='\t',header=False, index=False)
    return(postproc_preds)

def plot_pie_chart(x):    
    pt_5min = pt[(10*x):(10*x)+3000]
    pt_sum = [np.sum(pt_5min[i]) for i in range(8)]
    pt_dict = {classes[k]: pt_sum[k] for k in range(8)}

    labels = []
    sizes = []
    for x, y in pt_dict.items():
        labels.append(x)
        sizes.append(y)
    
    plt.figure(figsize=(5,5))
    plt.pie(sizes, labels=labels, autopct='%1.1f%%', textprops={'fontsize': 14})
    plt.axis('equal')
    plt.show()
    
def get_labelwise_audio(list_files,list_label_files,labels):    
    for label in labels:        
        start_time = 0                
        audio_label = []
        audio_label = np.array(audio_label)
        annotation_file = pd.DataFrame()

        for i in range(len(list_files)):            
            annotation_list = []
            annotation_list.append(start_time)
            audio_label = np.concatenate((audio_label,np.zeros(72000)),axis=0)
            start_time += 4.5
            annotation_list.append(start_time)
            annotation_list.append(list_files[i])
            annotation_file = annotation_file.append([annotation_list])

            total_duration,sr = get_audiolength(list_files[i])

            df = pd.read_excel(list_label_files[i])
            df_label = df.loc[df['Label']==label]
            df_label.reset_index(drop=True,inplace=True)

            for j in range(len(df_label)):
                annotation_list = []
                annotation_list.append(start_time)
                
                start_sample = time_to_sample(df_label['Start'][j],sr)
                stop_sample = time_to_sample(df_label['End'][j],sr)
                
                start_time = start_time+(df_label['End'][j]-df_label['Start'][j])
                annotation_list.append(start_time)
                annotation_label = str(np.round(df_label['Start'][j],2)) + '-' + str(np.round(df_label['End'][j],2))
                annotation_list.append(annotation_label)

                data, sr = sf.read(list_files[i],start=start_sample,stop=stop_sample)
                audio_label = np.concatenate((audio_label,data),axis=0)
                audio_label = np.concatenate((audio_label,np.zeros(8000)),axis=0)
                start_time += 0.5
                annotation_file = annotation_file.append([annotation_list])               

        audio_name = label + '.wav'    
        annotation_name = label + '_Annotation.txt'
        sf.write(audio_name,audio_label, sr)
        np.savetxt(annotation_name, annotation_file.values,fmt="%s", delimiter='\t')

def txt_to_excel(filename):
    df = pd.read_csv(filename,sep='\t') 
    to_append = df.columns
    df = pd.concat([pd.DataFrame([to_append], columns=df.columns), df], ignore_index=True)
    df.columns = ['Start','End','Label']    
    st = filename.split('.')[0] + '.xlsx'    
    df.to_excel(st,index = False)

def get_audiolength(filepath):
    data,sr = librosa.load(filepath,sr=None)
    total_duration = librosa.get_duration(y=data,sr=sr) 
    return(total_duration,sr)

def time_to_sample(time,samplerate):
    sample = time*samplerate
    return(int(np.floor(sample)))