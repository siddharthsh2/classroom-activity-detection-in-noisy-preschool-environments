
# Classroom Activity Detection In Noisy Preschool Environments

This project explores the application of speaker diarization and sound
event detection techniques to perform CAD on classroom recordings, in order
to mitigate the challenges posed by a small-scale and noisy dataset from
preschool classrooms, involving a high degree of speaker overlap.


## Project Organization

The project contains the following:
    
1. Audios_and_Annotation_Data.zip: Compressed folder containing the 24 classroom recordings used in the project, along with their manual annotated label tracks.
2. Classification_Paper.ipynb: Notebook consisting of code to implement the model architectures and post processing techniques proposed in this project.  
3. functions.py: Python file containing helper functions used in the project. 
## Preschool Classrooms Dataset
The preschool classroom dataset used in this project was provided by EarlySpark. The dataset is of a total duration of 3 hrs 39 min, containing recordings from 24 classroom sessions. These recordings
vary in duration between 4-22 min, with the mean session duration being 9 min 15 sec, recorded at a 16 kHz sampling rate.

This dataset was manually annotated using 10 labels, based on their relevance to the project, description for which can be found below:

1. Student/Child talking - CT: When a child is the dominant speaker (speaking alone/with background noise/some people talking in the background)

2. Teacher Talking - TT: When the teacher is the dominant speaker (speaking alone/with background noise/some people talking in the background)

3. Group Response - GR: Children speaking together in a synchronised/chorus response.

4. Multiple Speakers (Includes some speech) - MS: More than one speaker (speaking out of sync with each other), overlapping
speech where the dominant speaker cannot be identified, but with speech present and intelligible.

5. Silence - S: Region devoid of both speech activity and noise, or containing very low intensity noise

6. School Noise – SN: Students gossiping in a corner of class, students playing/yelling in the distant playground during recess break, presence of unintelligible speech unlike MS.

7. Background Noise - N: Non speech events like a car horn, noise from a TV/radio, construction work etc.

8. Teacher Yelling - TY: Separate from Teacher Talking (TT) in the sense that the teacher can clearly be heard shouting (in a loud voice) at the students as a method to detect corporal punishment.

9. Child Crying/Shouting - CC: Child crying or screaming /shouting (no speech); however, shouting does not include a child talking in a loud voice.

10. Striking - STR: Any event of clapping, jumping, stomping, banging the table etc. in conjunction with speech.

For the purpose of this project, only 6 labels are considered - CT, GR, MS, STR, TT and S. The labels SN and N are also combined into the Silence class, to be treated as a region with no speech activity and striking events, with possibility of ambient background noise.
## Model Architectures and Post Processing Methods

The notebook contains implementation of three model architectures - Log Mel Spectrogram Classifier, 2 Branch Classifier (using Log Mel Spectrogram and Wav2Vec2 features) and a 3 Branch Classifier (using Log Mel Spectrogram, Wav2Vec2 and OpenL3 features).

The notebook also contains a novel post proccesing framework that works via aggregation. It uses event boundaries generated using a Bayesian Information Criterion (BIC) based segmentation coupled with Root Mean Square (RMS) energy based silence detection. 
## Publication

 - [Siddhartha CV, Preeti Rao and Rajbabu Velmurugan (2023) ``Classroom Activity Detection in Noisy Preschool Environments with Audio Analysis", Proc. of ICSSES 2023, Tumkuru, India](https://www.ee.iitb.ac.in/course/~daplab/publications/2023/ICSSES_2023_Siddhartha_camera.pdf) 

## Authors

- Siddhartha CV (siddharthsh2@gmail.com), Prof. Preeti Rao (prao@ee.iitb.ac.in), Prof. Rajbabu Velmurugan (rajbabu@ee.iitb.ac.in) 

